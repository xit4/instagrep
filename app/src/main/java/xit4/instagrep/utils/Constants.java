package xit4.instagrep.utils;

/**
 * Created by xit on 12/04/17.
 */

public final class Constants {

    public static class Api{
        public static final String CLIENT_ID = "CLIENT_ID";
        public static final String CLIENT_SECRET = "CLIENT_SECRET";
        public static final String CALLBACK_URL = "CALLBACK_URL";
    }

    public static class exitCodes {
        public static int CODE_OK = 0;
        public static int CODE_KO = 1;
        public static int CODE_FETCH = 2;
    }

    public static class Misc{
        public static final String FRAGMENT_INDEX = "xit4.instagrep.FRAGMENT_INDEX";
        public static final String IMAGE_POSITION = "xit4.instagrep.IMAGE_POSITION";
        public static final String URL_INDEX = "xit4.instagrep.URL_INDEX";
        public static final String IMAGES_INDEX = "xit4.instagrep.IMAGES_INDEX";
    }

}
