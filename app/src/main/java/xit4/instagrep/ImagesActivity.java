package xit4.instagrep;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import xit4.instagrep.utils.Constants;

/**
 * Created by xit on 12/04/17.
 */

public class ImagesActivity extends FragmentActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int fragmentIndex = getIntent().getIntExtra(Constants.Misc.FRAGMENT_INDEX, 0);
        Fragment fragment;
        String tag;
        int titleRes;
        switch (fragmentIndex) {
            default:
            case GalleryFragment.INDEX:
                tag = GalleryFragment.class.getSimpleName();
                fragment = getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = new GalleryFragment();
                }
                titleRes = R.string.gallery_fragment_name;
                break;
            case SingleImageFragment.INDEX:
                tag = SingleImageFragment.class.getSimpleName();
                fragment = getSupportFragmentManager().findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = new SingleImageFragment();
                }
                titleRes = R.string.single_image_fragment_name;
                break;
        }

        setTitle(titleRes);
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, fragment, tag)
                .commit();
    }
}
