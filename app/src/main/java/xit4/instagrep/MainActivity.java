package xit4.instagrep;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.util.HashMap;

import xit4.instagrep.utils.Constants;
import xit4.instagrep.utils.Instagram;

import static xit4.instagrep.utils.Constants.exitCodes.CODE_KO;
import static xit4.instagrep.utils.Constants.exitCodes.CODE_OK;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnConnect,
    btnGallery,
    btnCrazyGallery,
    btnStupidGallery,
    btnDisconnect;
    private LinearLayout llAfterLoginView;
    Instagram insta;
    private HashMap<String, String> userInfo = new HashMap<String, String>();
    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == CODE_OK) {
                userInfo = insta.getUserInfo();
            } else if (msg.what == CODE_KO) {
                Toast.makeText(MainActivity.this, R.string.network_error,
                        Toast.LENGTH_SHORT).show();
                notConnectedButtons();
            }
            return false;
        }
    });


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(xit4.instagrep.R.layout.activity_main);

        initImageLoader(getApplicationContext());
        getWidgetReferences();
        bindEventHandlers();

        insta = new Instagram(MainActivity.this, Constants.Api.CLIENT_ID, Constants.Api.CLIENT_SECRET, Constants.Api.CALLBACK_URL);

        insta.setListener(new Instagram.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {
                connectedButtons();
            }

            @Override
            public void onFail(String error) {
                Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT)
                        .show();
            }
        });

        if (insta.hasAccessToken()) {
            connectedButtons();
            insta.fetchUserName(handler);
        } else {
            notConnectedButtons();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnConnect:
                insta.authorize();
                break;
            case R.id.btnDisconnect:
                final AlertDialog.Builder builder = new AlertDialog.Builder(
                        MainActivity.this);
                builder.setMessage(R.string.disconnect_dialog_msg)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        insta.resetAccessToken();
                                        notConnectedButtons();
                                    }
                                })
                        .setNegativeButton(R.string.no,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });
                final AlertDialog alert = builder.create();
                alert.show();
                break;
            case R.id.btnGallery:
                Intent intent = new Intent(this, ImagesActivity.class);

                String url = "https://api.instagram.com/v1/users/"
                        + userInfo.get(Instagram.TAG_ID)
                        + "/media/recent?access_token=" + insta.getTOken();
                intent.putExtra(Constants.Misc.URL_INDEX, url);
                intent.putExtra(Constants.Misc.FRAGMENT_INDEX, GalleryFragment.INDEX);
                startActivity(intent);
                break;
            case R.id.btnCrazyGallery:
                Toast.makeText(MainActivity.this, R.string.crazy_gallery_toast,Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnStupidGallery:
                Toast.makeText(MainActivity.this, R.string.stupid_gallery_toast,Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void notConnectedButtons(){
        btnConnect.setVisibility(View.VISIBLE);
        btnDisconnect.setVisibility(View.GONE);
        llAfterLoginView.setVisibility(View.GONE);
    }

    private void connectedButtons(){
        btnConnect.setVisibility(View.GONE);
        btnDisconnect.setVisibility(View.VISIBLE);
        llAfterLoginView.setVisibility(View.VISIBLE);
    }

    private void getWidgetReferences(){
        setBtnConnect((Button) findViewById(R.id.btnConnect));
        setBtnDisconnect((Button) findViewById(R.id.btnDisconnect));
        setBtnGallery((Button) findViewById(R.id.btnGallery));
        setBtnCrazyGallery((Button) findViewById(R.id.btnCrazyGallery));
        setBtnStupidGallery((Button) findViewById(R.id.btnStupidGallery));
        setLlAfterLoginView((LinearLayout) findViewById(R.id.llAfterLoginView));
    }

    private void bindEventHandlers() {
        btnConnect.setOnClickListener(this);
        btnDisconnect.setOnClickListener(this);
        btnGallery.setOnClickListener(this);
        btnCrazyGallery.setOnClickListener(this);
        btnStupidGallery.setOnClickListener(this);
    }

    public static void initImageLoader(Context context) {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // TODO Remove for release app

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

    public Button getBtnConnect() {
        return btnConnect;
    }

    public void setBtnConnect(Button btnConnect) {
        this.btnConnect = btnConnect;
    }

    public Button getBtnGallery() {
        return btnGallery;
    }

    public void setBtnGallery(Button btnGallery) {
        this.btnGallery = btnGallery;
    }

    public LinearLayout getLlAfterLoginView() {
        return llAfterLoginView;
    }

    public void setLlAfterLoginView(LinearLayout llAfterLoginView) {
        this.llAfterLoginView = llAfterLoginView;
    }

    public Button getBtnCrazyGallery() {
        return btnCrazyGallery;
    }

    public void setBtnCrazyGallery(Button btnCrazyGallery) {
        this.btnCrazyGallery = btnCrazyGallery;
    }

    public Button getBtnStupidGallery() {
        return btnStupidGallery;
    }

    public void setBtnStupidGallery(Button btnStupidGallery) {
        this.btnStupidGallery = btnStupidGallery;
    }

    public void setBtnDisconnect(Button btnDisconnect) {
        this.btnDisconnect = btnDisconnect;
    }
}
