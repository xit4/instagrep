package xit4.instagrep;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import xit4.instagrep.utils.Constants;
import xit4.instagrep.utils.JSONParser;

import static xit4.instagrep.utils.Constants.exitCodes.CODE_FETCH;
import static xit4.instagrep.utils.Constants.exitCodes.CODE_KO;

public class GalleryFragment extends Fragment {

    public static final int INDEX = 0;

    private String url;

    public static final String TAG_DATA = "data";
    public static final String TAG_IMAGES = "images";
    public static final String TAG_THUMBNAIL = "thumbnail";
    public static final String TAG_STANDARD_RESOLUTION = "standard_resolution";
    public static final String TAG_URL = "url";
    private List<String> imageThumbnails = new ArrayList<>();
    private List<String> images = new ArrayList<>();
    ProgressDialog pd;
    GridView listView;

    private Handler handler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (pd != null && pd.isShowing())
                pd.dismiss();
            if (msg.what == CODE_FETCH) {
                setImageGridAdapter();
            } else {
                Toast.makeText(getActivity().getApplicationContext(), R.string.network_error,
                        Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
            return false;
        }
    });

    public GalleryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
        listView = (GridView) rootView.findViewById(R.id.galleryGrid);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ImagesActivity.class);
                intent.putExtra(Constants.Misc.FRAGMENT_INDEX, SingleImageFragment.INDEX);
                intent.putExtra(Constants.Misc.IMAGE_POSITION, position);
                intent.putStringArrayListExtra(Constants.Misc.IMAGES_INDEX, (ArrayList<String>) images);
                startActivity(intent);
            }
        });

        url = getActivity().getIntent().getStringExtra(Constants.Misc.URL_INDEX);

        getAllMediaImages();
        return rootView;
    }

    private class ImageAdapter extends BaseAdapter {

        private LayoutInflater inflater;

        private DisplayImageOptions options;

        ImageAdapter(Context context) {
            inflater = LayoutInflater.from(context);

            options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .build();
        }

        @Override
        public int getCount() {
            return imageThumbnails.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View view = convertView;
            if (view == null) {
                view = inflater.inflate(R.layout.item_grid_image, parent, false);
                holder = new ViewHolder();
                assert view != null;
                holder.imageView = (ImageView) view.findViewById(R.id.image);
                holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            ImageLoader.getInstance()
                    .displayImage(imageThumbnails.get(position), holder.imageView, options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            holder.progressBar.setProgress(0);
                            holder.progressBar.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    }, new ImageLoadingProgressListener() {
                        @Override
                        public void onProgressUpdate(String imageUri, View view, int current, int total) {
                            holder.progressBar.setProgress(Math.round(100.0f * current / total));
                        }
                    });

            return view;
        }
    }

    private void setImageGridAdapter() {
        listView.setAdapter(new ImageAdapter(getActivity()));
    }

    private void getAllMediaImages() {
        pd = ProgressDialog.show(getActivity(), "", getString(R.string.loading_images_msg));
        new Thread(new Runnable() {

            @Override
            public void run() {
                int exitCode = CODE_FETCH;
                try {
                    JSONParser jsonParser = new JSONParser();
                    JSONObject jsonObject = jsonParser
                            .getJSONFromUrlByGet(url);
                    JSONArray data = jsonObject.getJSONArray(TAG_DATA);
                    for (int data_i = 0; data_i < data.length(); data_i++) {
                        JSONObject data_obj = data.getJSONObject(data_i);

                        JSONObject images_obj = data_obj
                                .getJSONObject(TAG_IMAGES);

                        JSONObject thumbnail_obj = images_obj
                                .getJSONObject(TAG_THUMBNAIL);

                        JSONObject standard_obj = images_obj
                                .getJSONObject(TAG_STANDARD_RESOLUTION);

                        imageThumbnails.add(thumbnail_obj.getString(TAG_URL));
                        images.add(standard_obj.getString(TAG_URL));
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                    exitCode = CODE_KO;
                }
                handler.sendEmptyMessage(exitCode);
            }
        }).start();
    }

    static class ViewHolder {
        ImageView imageView;
        ProgressBar progressBar;
    }

}
