README
====================

This android app uses the instagram API and the Android Universal Image Loader to build a simple gallery.

Pull the project in Android Studio and it should be already runnable. In case it is not, try closing the project and reopening it.

In any case **before deploying the application** the CLIENT_ID, CLIENT_SECRET and CALLBACK_URL **must be changed** to enable the Instagram integration.

These constants are contained in the *xit4.instagrep.utils.Constants* class